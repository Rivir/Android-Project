package com.example.root.layout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        textView2 = (TextView)findViewById(R.id.textView2);

        Bundle bundle = this.getIntent().getExtras();
        String name=bundle.getString("name");
        String birthday = bundle.getString("birthday");
        String sex=bundle.getString("sex");
        String interest=bundle.getString("interest");
        String grade=bundle.getString("grade");
        textView2.setText(name+"\n"+birthday+"\n"+sex+"\n"+interest+"\n"+grade);
    }
}
