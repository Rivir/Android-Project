package com.example.root.layout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private EditText editText1;
    private RadioButton radioFaMale;
    private RadioButton radioMale;
    private CheckBox checkBox1;
    private CheckBox checkBox2;
    private EditText editBirthday;
    private DatePicker datePicker;
    private Button calbutton;
    private Button calbutton2;
    private Spinner spin;
    private String grade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText1 = (EditText)findViewById(R.id.editText); //先注册R才能使用
        radioFaMale = (RadioButton)findViewById(R.id.radioFemale);
        radioMale = (RadioButton)findViewById(R.id.radioMale);
        checkBox1 = (CheckBox)findViewById(R.id.checkBox1);
        checkBox2 = (CheckBox)findViewById(R.id.checkBox2);
        editBirthday = (EditText)findViewById(R.id.editTextBirthday);
        datePicker = (DatePicker)findViewById(R.id.datePicker);
        editBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker.setVisibility(View.VISIBLE);
            }
        });

        datePicker.init(2013,8,20,new DatePicker.OnDateChangedListener(){
            @Override
            public void onDateChanged(DatePicker view,int year,int monthOfYear,int dayOfMonth){
                Calendar calendar = Calendar.getInstance();
                calendar.set(year,monthOfYear,dayOfMonth);
                SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
                editBirthday.setText(format.format(calendar.getTime()));
                datePicker.setVisibility(View.INVISIBLE);
            }
        });

        calbutton = (Button)findViewById(R.id.button2);
        calbutton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String sex = "";
                if (radioMale.isChecked()) {
                    sex = "男";
                } else if (radioFaMale.isChecked()) {
                    sex = "女";
                }
                if(checkBox1.isChecked()){
                    sex += " 足球";
                }
                if(checkBox2.isChecked()){
                    sex += " 篮球";
                }
                Toast.makeText(MainActivity.this, "你选择是:" + sex, Toast.LENGTH_SHORT).show();

            }
        });

//        calbutton2 = (Button)findViewById(R.id.button2);
//        calbutton2.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view){
//                String intrest = "";
//                if(checkBox1.isChecked()){
//                    intrest += "足球";
//                }
//                if(checkBox2.isChecked()){
//                    intrest += "篮球";
//                }
//                Toast.makeText(MainActivity.this,"你选择是:"+intrest,Toast.LENGTH_SHORT).show();
//            }
//        });

        calbutton = (Button)findViewById(R.id.button2);
        calbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent();
                intent2.setClass(MainActivity.this,SecondActivity.class);
                startActivity(intent2);
            }
        });




        Spinner spin = (Spinner)findViewById(R.id.spinner4);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.grade,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView,View view,int i,long l){
                String[] grades = getResources().getStringArray(R.array.grade);
                grade = grades[i];
                Toast.makeText(MainActivity.this,"你点击的是:"+grades[i],Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){

            }
        });

        calbutton = (Button)findViewById(R.id.button2);
        calbutton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String name=editText1.getText().toString();
                String birthday = editBirthday.getText().toString();
                String sex = radioMale.isChecked()?"男":"女";
                String interest = "";
                if(checkBox1.isChecked()) interest+="足球";
                if(checkBox2.isChecked()) interest+="篮球";

                Intent intent2 = new Intent();
                intent2.setClass(MainActivity.this,SecondActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("Name","feng88724");
                bundle.putString("name",name);
                bundle.putString("birthday",birthday);
                bundle.putString("sex",sex);
                bundle.putString("interest",interest);
                bundle.putString("grade",grade);
                intent2.putExtras(bundle);
                startActivity(intent2);
            }
        });
    }
}
